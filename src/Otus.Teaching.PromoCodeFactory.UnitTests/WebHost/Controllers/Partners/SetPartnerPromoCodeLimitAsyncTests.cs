﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IDisposable
    {
        private readonly Mock<IRepository<Partner>> _mockPartnerRepository;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _mockPartnerRepository = new Mock<IRepository<Partner>>(); 
        }
        [Fact]
        public async void PartnersController_NotFound_HttpNotFoundResponse()
        {
            _mockPartnerRepository.Setup(s => s.GetByIdAsync(Guid.Empty)).Returns(Task.FromResult<Partner>(null));
            var partnerCont = GetPartnersController(_mockPartnerRepository.Object);

            var res = await partnerCont.GetPartnerAsync(Guid.Empty);
            res.Result.Should().BeOfType<NotFoundResult>();
        }
        
        [Fact]
        public async void PartnersController_PartnerIsBlocked_BadRequestResponse()
        {
            _mockPartnerRepository.Setup(s => s.GetByIdAsync(Guid.Empty)).Returns(Task.FromResult(new PartnerBuilder().Disactived().Build()));
            var partnerCont = GetPartnersController(_mockPartnerRepository.Object);

            var res = await partnerCont.GetPartnerAsync(Guid.Empty);
            res.Result.Should().BeOfType<BadRequestResult>();
        }
        
        [Fact]
        public async void PartnersController_SetGoodLimit_CounterDropToNull()
        {
            var par = new PartnerBuilder().WithNumberIssuedPromoCodes(43).Build();
            _mockPartnerRepository.Setup(s => s.GetByIdAsync(Guid.Empty)).Returns(Task.FromResult(par));

            var partCont = GetPartnersController(_mockPartnerRepository.Object);
            var req =  new SetPartnerPromoCodeLimitRequestBuilder()
                .WithEndDate(DateTime.Now + TimeSpan.FromDays(1))
                .Build();
            await partCont.SetPartnerPromoCodeLimitAsync(Guid.Empty, req);
            _mockPartnerRepository.Verify(m=>m.UpdateAsync(par),Times.Once);
            par.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        [Fact]
        public async void PartnersController_SetOldLimit_CounterDontDropToNull()
        {
            var par = new PartnerBuilder().WithNumberIssuedPromoCodes(43).Build();
            _mockPartnerRepository.Setup(s => s.GetByIdAsync(Guid.Empty)).Returns(Task.FromResult(par));
            var partCont = GetPartnersController(_mockPartnerRepository.Object);
            var req = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithEndDate(DateTime.Now - TimeSpan.FromDays(1))
                .Build();
            await partCont.SetPartnerPromoCodeLimitAsync(Guid.Empty, req);
            _mockPartnerRepository.Verify(m=>m.UpdateAsync(par),Times.Once);
            par.NumberIssuedPromoCodes.Should().Be(43);
        }
        
        [Fact]
        public async void PartnersController_SetLimit_DisablePreviousLimit()
        {
            var promoCode = new PartnerPromoCodeLimitBuilder().Build();
            var par = new PartnerBuilder().WithNumberIssuedPromoCodes(43).WithPartnerPromoCodeLimit(promoCode).Build();
            _mockPartnerRepository.Setup(s => s.GetByIdAsync(Guid.Empty)).Returns(Task.FromResult(par));
            var partCont = GetPartnersController(_mockPartnerRepository.Object);
            var req =  new SetPartnerPromoCodeLimitRequestBuilder()
                .WithEndDate(DateTime.Now + TimeSpan.FromDays(1))
                .Build();
            await partCont.SetPartnerPromoCodeLimitAsync(Guid.Empty, req);
            _mockPartnerRepository.Verify(m=>m.UpdateAsync(par),Times.Once);
            var prevLimit = par.PartnerLimits.FirstOrDefault(s => s.Id == promoCode.Id);
            prevLimit.Should().NotBeNull();
            prevLimit.CancelDate.HasValue.Should().BeTrue();
        }
        
        [Fact]
        public async void PartnersLimit_SetLimit_LimitConstraint()
        {
            var par = new PartnerBuilder().Build();
            _mockPartnerRepository.Setup(s => s.GetByIdAsync(Guid.Empty)).Returns(Task.FromResult(par));
            var partCont = GetPartnersController(_mockPartnerRepository.Object);
            var req = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithEndDate(DateTime.Now + TimeSpan.FromDays(1))
                .WithLimit(0).Build();
            var res  = await partCont.SetPartnerPromoCodeLimitAsync(Guid.Empty, req);
            res.Should().BeOfType<BadRequestObjectResult>();
            ((BadRequestObjectResult)res).Value.Should().BeEquivalentTo("Лимит должен быть больше 0");
        }
        
        [Fact]
        public async void PartnersLimit_AddLimit_CheckLimitAdded()
        {
            var par = new PartnerBuilder().Build();
            _mockPartnerRepository.Setup(s => s.GetByIdAsync(Guid.Empty)).Returns(Task.FromResult(par));
            var partCont = GetPartnersController(_mockPartnerRepository.Object);
            var req =  new SetPartnerPromoCodeLimitRequestBuilder()
                .WithEndDate(DateTime.Now + TimeSpan.FromDays(1))
                .WithLimit(43).Build();
            await partCont.SetPartnerPromoCodeLimitAsync(Guid.Empty, req);
            //прверяем что метод обновления бд был вызван, так как сущности связаны в EF,
            //то после сохранения изменеий будет добавлена и новая сущность PartnerPromoCodeLimit
            //поэтому проверяем вызывался ли метод обновления сущности партнера и был ли у него в этот момент нвый лмимт
            _mockPartnerRepository.Verify(m=>m.UpdateAsync(par),Times.Once);
            par.PartnerLimits.Count.Should().Be(1);
            
            par.PartnerLimits.First().Limit.Should().Be(req.Limit);

        }

        private PartnersController GetPartnersController(IRepository<Partner> repository)
        {
            return new PartnersController(repository);
        }
        
        public void Dispose()
        {
        }
    }
}