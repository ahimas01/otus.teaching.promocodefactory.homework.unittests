using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerPromoCodeLimitBuilder
    {
        private int _limit=100;

        public PartnerPromoCodeLimitBuilder WithLimit(int limit)
        {
            _limit = limit;
            return this;
        }
        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit
            {
                Id = Guid.NewGuid(), Limit = _limit, CreateDate = DateTime.Now - TimeSpan.FromDays(1),
            };
        }
   
    }
}