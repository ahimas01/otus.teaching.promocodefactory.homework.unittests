using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitRequestBuilder
    {
        private int _limit = 4;
        private DateTime _endDate;

        public SetPartnerPromoCodeLimitRequestBuilder WithLimit(int limit)
        {
            _limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitRequestBuilder WithEndDate(DateTime endTime)
        {
            _endDate = endTime;
            return this;
        }
        public SetPartnerPromoCodeLimitRequest Build()
        {
            return new SetPartnerPromoCodeLimitRequest { Limit =_limit,EndDate = _endDate};
        }
    }
}