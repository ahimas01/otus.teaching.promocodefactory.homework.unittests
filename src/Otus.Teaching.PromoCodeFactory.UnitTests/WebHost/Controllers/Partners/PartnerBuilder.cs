using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private bool _isActive = true;
        private int _numberIssuedPromoCodes =0;
        private List<PartnerPromoCodeLimit> _limits = new List<PartnerPromoCodeLimit>();

        public PartnerBuilder Disactived()
        {
            _isActive = false;
            return this;
        }
        
        public PartnerBuilder WithNumberIssuedPromoCodes(int count)
        {
            _numberIssuedPromoCodes = count;
            return this;
        }
        
        public PartnerBuilder WithPartnerPromoCodeLimit(PartnerPromoCodeLimit promoCodeLimit)
        {
            _limits.Add(promoCodeLimit);
            return this;
        }
        public Partner Build()
        {
            return new Partner()
            {
                Id = Guid.NewGuid(), Name = "Name", IsActive = _isActive,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes, PartnerLimits = _limits
            };
        }
    }
}